﻿using MainDataAndVariablesDatabaseMerger.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainDataAndVariablesDatabaseMerger
{
    class Program
    {
        private const string DATA_FOLDER = "Data";
        private const string VARIABLES_FOLDER = "Variables";

        private const string INPUT_FOLDER = "Input";
        private const string OUTPUT_FOLDER = "Output";
        private const string OUTPUT_FILENAME = "database_{0}.{1}";
        private const char CSV_DIVIDER = ';';

        private static string _mainDataName;
        private static int shiftPeriods = 0;
        private static bool exportArff = false;

        static void Main(string[] args)
        {
            try
            {
                for(int i = 0; i <= shiftPeriods; i++)
                {
                    List<MainData> mainData = ImportMainData();

                    List<Variable> variables = ImportVariables();

                    List<List<string>> output = Merge(mainData, variables);

                    ShiftMainData(output, i);

                    if(exportArff)
                        ExportArff(output, string.Format(OUTPUT_FILENAME, i, "arff"));

                    ExportCsv(output, string.Format(OUTPUT_FILENAME, i, "csv"));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
            }

            Console.Write("Press any key to continue . . .");
            Console.ReadKey(true);
        }

        private static void ShiftMainData(List<List<string>> output, int periodInMonths)
        {
            if (periodInMonths > 0)
            {
                int lastColumnIndex = output[0].Count() - 1;

                // 1 to jump the header
                for (int i = 1; i < output.Count() - periodInMonths; i++)
                {
                    int shiftRowIndex = i + periodInMonths;
                    output[i].RemoveAt(lastColumnIndex);
                    output[i].Add(output[shiftRowIndex].Last());
                }

                output.RemoveRange(output.Count() - 1 - periodInMonths, periodInMonths);
            }
        }

        private static void ExportCsv(List<List<string>> output, string filenameCsv)
        {
            Console.WriteLine(string.Format("Creating '{0}'...", filenameCsv));

            string outputDirectory = Path.Combine(Directory.GetCurrentDirectory(), DATA_FOLDER, OUTPUT_FOLDER);
            Directory.CreateDirectory(outputDirectory);
            string outputFile = Path.Combine(outputDirectory, filenameCsv);
            
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(outputFile))
            {
                string line = string.Empty;
                foreach (string headerValue in output.First())
                    line += string.Format("\"{0}\"{1}", headerValue.Replace('.', '_').Replace(',', '_'), CSV_DIVIDER);
                line = line.Remove(line.Count() - 1);
                file.WriteLine(line);

                foreach (List<string> row in output.Skip(1))
                {
                    line = string.Empty;
                    foreach (string value in row)
                        line += (value.Replace(',', '.') + CSV_DIVIDER);

                    line = line.Remove(line.Count() - 1);

                    file.WriteLine(line);
                }
                
                file.Close();
                file.Dispose();
            }

            Console.WriteLine(string.Format("Exported to '{0}'", outputFile));
        }

        private static void ExportArff(List<List<string>> output, string filenameArff)
        {
            Console.WriteLine(string.Format("Creating '{0}'...", filenameArff));

            string outputDirectory = Path.Combine(Directory.GetCurrentDirectory(), DATA_FOLDER, OUTPUT_FOLDER);
            Directory.CreateDirectory(outputDirectory);
            string outputFile = Path.Combine(outputDirectory, filenameArff);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(outputFile))
            {
                file.WriteLine("@RELATION " + _mainDataName);
                file.WriteLine(string.Empty);

                foreach(string headerValue in output.First())
                    file.WriteLine("@ATTRIBUTE " + headerValue.Replace('.', '_').Replace(',', '_') + " REAL");

                file.WriteLine(string.Empty); 
                file.WriteLine("@DATA");
                file.WriteLine(string.Empty);

                foreach (List<string> row in output.Skip(1))
                {
                    string line = string.Empty;
                    foreach (string value in row)
                        line += (value + ",");

                    line = line.Remove(line.Count() - 1);

                    file.WriteLine(line);
                }

                file.WriteLine(string.Empty);
                file.WriteLine("%");
                file.WriteLine("%");
                file.WriteLine("%");

                file.Close();
                file.Dispose();
            }

            Console.WriteLine(string.Format("Exported to '{0}'", outputFile));
        }

        private static List<List<string>> Merge(List<MainData> mainDatas, List<Variable> variables)
        {
            Console.WriteLine("Merging databases...");

            Console.WriteLine("Removing incompatible data...");

            //Remove the mainDatas that are older than the oldest variable
            Variable minVariable = variables.OrderByDescending(v => int.Parse(v.Year)).ThenByDescending(v => int.Parse(v.Month)).Last();
            mainDatas.RemoveAll(s => int.Parse(s.Year) < int.Parse(minVariable.Year) ||
            (int.Parse(s.Year) == int.Parse(minVariable.Year) && int.Parse(s.Month) < int.Parse(minVariable.Month)));

            //Remove the variables that are older than the oldest mainData
            MainData minMainData = mainDatas.OrderByDescending(s => int.Parse(s.Year)).ThenByDescending(s => int.Parse(s.Month)).Last();
            variables.RemoveAll(v => int.Parse(v.Year) < int.Parse(minMainData.Year) ||
            (int.Parse(v.Year) == int.Parse(minMainData.Year) && int.Parse(v.Month) < int.Parse(minMainData.Month)));

            //Remove the mainDatas that are newer than the newest variable
            Variable maxVariable = variables.OrderByDescending(v => int.Parse(v.Year)).ThenByDescending(v => int.Parse(v.Month)).First();
            mainDatas.RemoveAll(s => int.Parse(s.Year) > int.Parse(maxVariable.Year) ||
            (int.Parse(s.Year) == int.Parse(maxVariable.Year) && int.Parse(s.Month) > int.Parse(maxVariable.Month)));

            //Remove the variables that are newer than the newest mainData
            MainData maxMainData = mainDatas.OrderByDescending(s => int.Parse(s.Year)).ThenByDescending(s => int.Parse(s.Month)).First();
            variables.RemoveAll(v => int.Parse(v.Year) > int.Parse(maxMainData.Year) ||
            (int.Parse(v.Year) == int.Parse(maxMainData.Year) && int.Parse(v.Month) > int.Parse(maxMainData.Month)));

            Console.WriteLine("Incompatible data removed");

            Console.WriteLine("Creating database structure...");

            List<string> headers = new List<string>();
            headers.Add("year");
            headers.Add("month");

            var filteredVariables = variables.Select(v => new { v.Type, v.LocationLabel }).Distinct();
            foreach(var variable in filteredVariables)
            {
                headers.Add(string.Format("{0}_{1}", variable.Type, variable.LocationLabel));
            }

            headers.Add(_mainDataName);

            List<List<string>> rows = new List<List<string>>();
            rows.Add(headers);

            foreach (MainData mainData in mainDatas)
            {
                List<string> row = new List<string>();
                row.Add(mainData.Year);
                row.Add(mainData.Month);

                List<Variable> matchVariables = variables.Where(v => v.Year == mainData.Year && v.Month == mainData.Month).ToList();

                foreach (Variable matchVariable in matchVariables)
                {
                    row.Add(matchVariable.Value);
                }

                row.Add(mainData.Value);

                rows.Add(row);
            }
            Console.WriteLine("Database structure created");

            Console.WriteLine("Databases merged");

            return rows;
        }

        private static List<MainData> ImportMainData()
        {
            DirectoryInfo d = new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory(), DATA_FOLDER, INPUT_FOLDER));
            FileInfo[] files = d.GetFiles("*.csv");

            if (files.Count() > 1 || files.Count() == 0)
                throw new Exception("Only one main database is supported.");

            string mainDataFileName = files[0].Name;
            _mainDataName = Path.GetFileNameWithoutExtension(mainDataFileName);

            Console.WriteLine(string.Format("Importing database from '{0}'", mainDataFileName));

            List<MainData> mainDatas = new List<MainData>();

            string[] lines = File.ReadAllLines(files[0].FullName).Skip(1).ToArray();

            foreach(string line in lines)
            {
                string[] lineArray = line.Split(';');

                for(int i = 1; i < lineArray.Count(); i++)
                {
                    MainData mainData = new MainData();
                    mainData.Year = lineArray[0].Trim();
                    mainData.Month = i.ToString().Trim();
                    mainData.Value = lineArray[i].Trim();

                    mainDatas.Add(mainData);
                }
            }

            Console.WriteLine("Main database imported");

            return mainDatas;
        }

        public static List<Variable> ImportVariables()
        {
            Console.WriteLine("Importing variables database...");

            DirectoryInfo d = new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory(), DATA_FOLDER, INPUT_FOLDER, VARIABLES_FOLDER));
            FileInfo[] files = d.GetFiles("*.csv");
            
            List<Variable> variables = new List<Variable>();

            foreach (FileInfo file in files)
            {
                Console.WriteLine(string.Format("from '{0}'", file));

                string[] lines = File.ReadAllLines(file.FullName);
                string[] headerDates = lines[0].Split(';');

                lines = lines.Skip(1).ToArray();

                foreach (string line in lines)
                {
                    string[] lineArray = line.Split(';');

                    for(int i = 1; i < lineArray.Count(); i++)
                    {
                        Variable variable = new Variable();
                        variable.Type = Path.GetFileNameWithoutExtension(file.Name);
                        variable.LocationLabel = lineArray[0].Trim();
                        variable.Month = Utils.GetMonth(headerDates[i]).Trim();
                        variable.Year = Utils.GetYear(headerDates[i]).Trim();
                        variable.Value = lineArray[i].Trim();

                        variables.Add(variable);
                    }
                }
            }

            Console.WriteLine("Variables databases imported");

            return variables;
        }
    }
}
