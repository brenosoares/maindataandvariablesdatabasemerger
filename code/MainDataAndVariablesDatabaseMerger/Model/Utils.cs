﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainDataAndVariablesDatabaseMerger.Model
{
    public static class Utils
    {
        public static string GetYear(string v)
        {
            return v.Split('/')[1];
        }

        public static string GetMonth(string v)
        {
            switch (v.Split('/')[0])
            {
                case "Jan":
                    return "1";
                case "Fev":
                    return "2";
                case "Mar":
                    return "3";
                case "Abr":
                    return "4";
                case "Maio":
                    return "5";
                case "Jun":
                    return "6";
                case "Jul":
                    return "7";
                case "Ago":
                    return "8";
                case "Set":
                    return "9";
                case "Out":
                    return "10";
                case "Nov":
                    return "11";
                case "Dez":
                    return "12";
                default:
                    throw new Exception("Month not recognized.");
            }
        }
    }
}
