﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainDataAndVariablesDatabaseMerger.Model
{
    public class Variable
    {
        public string Type { get; set; }
        public string LocationLabel { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public string Value { get; set; }
    }
}
