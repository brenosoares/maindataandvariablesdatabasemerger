﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainDataAndVariablesDatabaseMerger
{
    public class MainData
    {
        public string Year { get; set; }
        public string Month { get; set; }
        public string Value { get; set; }
    }
}
