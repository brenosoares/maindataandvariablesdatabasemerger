﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoveColumns
{
    class Program
    {
        private const string INPUT_FILENAME = "columns_to_filter.txt";

        static void Main(string[] args)
        {
            try
            {
                string inputFilePath = Path.Combine(Directory.GetCurrentDirectory(), INPUT_FILENAME);
                Console.WriteLine("Reading {0}", inputFilePath);
                string[] inputFileLines = File.ReadAllLines(inputFilePath);

                string databaseFilename = inputFileLines[0];

                string databaseFilePath = Path.Combine(Directory.GetCurrentDirectory(), databaseFilename);
                Console.WriteLine("Reading {0}", databaseFilePath);
                string[] databaseFileLines = File.ReadAllLines(databaseFilePath);
                List<string> databaseHeader = databaseFileLines[0].Split(',').ToList();

                List<int> columnsIndexes = new List<int>();

                List<string> columns = databaseHeader.Take(databaseHeader.Count() - 1).ToList();

                Console.WriteLine("Selecting columns...");

                for (int i = 0; i < columns.Count(); i++)
                {
                    if (!inputFileLines.Skip(1).Any(c => columns[i].Contains(c)))
                        columnsIndexes.Add(i);
                }

                string outputFilePath = Path.Combine(Directory.GetCurrentDirectory(), "filtered_" + databaseFilename);
                using (System.IO.StreamWriter outputFile = new System.IO.StreamWriter(outputFilePath))
                {
                    Console.WriteLine("Removing columns...");
                    foreach (string databaseFileLine in databaseFileLines)
                    {
                        List<string> line = databaseFileLine.Split(',').ToList();

                        for(int i = 0; i < line.Count; i++)
                            if (columnsIndexes.Contains(i))
                                line[i] = string.Empty;

                        line.RemoveAll(c => c.Equals(string.Empty));

                        string newLine = string.Empty;
                        foreach (string value in line)
                            newLine += (value + ',');

                        newLine = newLine.Remove(newLine.Count() - 1);

                        outputFile.WriteLine(newLine);
                    }

                    outputFile.Close();
                    outputFile.Dispose();
                }

                Console.WriteLine("Filtered database successfully saved to: {0}", outputFilePath);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
            }

            Console.Write("Press any key to continue . . .");
            Console.ReadKey(true);
        }
    }
}
