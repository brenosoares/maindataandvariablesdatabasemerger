﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilterDatabaseByCorrelation
{
    class Program
    {
        
        private const double THRESHOLD = 0.5;
        private const string OUTPUT_FILENAME = "_correlation";
        private const string REMOVED_COLUMNS_FILENAME = "removed_columns.txt";

        static void Main(string[] args)
        {
            try
            {
                DirectoryInfo d = new DirectoryInfo(Directory.GetCurrentDirectory());
                FileInfo[] files = d.GetFiles("*.csv");

                foreach (FileInfo file in files)
                {
                    string filenameCsv = string.Format("{0}{1}{2}", Path.GetFileNameWithoutExtension(file.Name), OUTPUT_FILENAME, ".csv");

                    Console.WriteLine(string.Format("Creating '{0}'", filenameCsv));

                    string outputFilePath = Path.Combine(Directory.GetCurrentDirectory(), filenameCsv);

                    using (System.IO.StreamWriter outputFile = new System.IO.StreamWriter(outputFilePath))
                    {
                        string[] lines = File.ReadAllLines(file.FullName).ToArray();
                        int matrixRowCount = lines.Skip(1).Count();
                        int matrixColumnCount = lines[0].Split(',').Count();
                        
                        double[,] matrixArray = new double[matrixRowCount, matrixColumnCount];

                        for (int i = 0; i < matrixRowCount; i++)
                        {
                            string[] values = lines.Skip(1).ElementAt(i).Split(',');
                            for (int j = 0; j < values.Count(); j++)
                            {
                                matrixArray[i,j] = double.Parse(values[j]);
                            }
                        }

                        Matrix<double> matrix = Matrix<double>.Build.DenseOfArray(matrixArray);

                        Console.WriteLine("Normalizing data");
                        Matrix<double> normalizedMatrix = matrix.NormalizeColumns(1);

                        Console.WriteLine("Calculating correlation");

                        Matrix<double> correlationMatrix = MathNet.Numerics.Statistics.Correlation.PearsonMatrix(normalizedMatrix.ToColumnArrays());

                        string[] header = lines[0].Split(',');
                        int[] columnsIndexesToRemove = GetIndexesToRemove(correlationMatrix, header, Path.GetFileNameWithoutExtension(filenameCsv));

                        foreach (string line in lines)
                        {
                            List<string> splittedLine = line.Split(',').ToList();

                            for(int i = 0; i < columnsIndexesToRemove.Count(); i++)
                            {
                                splittedLine.RemoveAt(columnsIndexesToRemove[i] - i);
                            }

                            string newLine = string.Empty;

                            foreach (string value in splittedLine)
                                newLine += (value + ',');

                            newLine = newLine.Remove(newLine.Count() - 1);

                            outputFile.WriteLine(newLine);
                        }

                        outputFile.Close();
                        outputFile.Dispose();
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
            }

            Console.Write("Press any key to continue . . .");
            Console.ReadKey(true);
        }

        private static int[] GetIndexesToRemove(Matrix<double> m, string[] header, string filename)
        {
            string outputFilePath = Path.Combine(Directory.GetCurrentDirectory(), string.Format("{0}_{1}", filename, REMOVED_COLUMNS_FILENAME));

            Console.WriteLine(string.Format("The removed columns due having correlation below |{0}| are being saved to '{1}'", THRESHOLD, outputFilePath));

            List<int> indexes = new List<int>();
         
            using (System.IO.StreamWriter outputFile = new System.IO.StreamWriter(outputFilePath))
            {
                // i=2 to avoid removing year and month
                for (int i = 2; i < m.ColumnCount - 1; i++)
                {
                    double correlation = m[m.RowCount - 1, i];
                    if (Math.Abs(correlation) < THRESHOLD)
                    {
                        indexes.Add(i);
                        outputFile.WriteLine(string.Format("{0} => C={1}", header[i], correlation));
                    }
                }

                outputFile.Close();
                outputFile.Dispose();
            }

            return indexes.ToArray();
        }
    }
}
