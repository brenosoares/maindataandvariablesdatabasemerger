﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShiftPeriodsSameDatabase
{
    public class Program
    {
        private const string OUTPUT_FILENAME = "database_3_months.csv";
        private const string INPUT_FILENAME = "database.csv";
        private const string OUTPUT_HEADER_NAME = ",output_{0}";
        private static int periodsAhead = 3;

        static void Main(string[] args)
        {
            try
            {
                string inputFilePath = Path.Combine(Directory.GetCurrentDirectory(), INPUT_FILENAME);
                Console.WriteLine("Reading {0}", inputFilePath);
                string[] inputFileLines = File.ReadAllLines(inputFilePath);

                List<List<string>> output = new List<List<string>>();
                foreach (string line in inputFileLines)
                {
                    output.Add(line.Split(',').ToList());
                }

                ShiftOutput(output, periodsAhead);

                ExportCsv(output, OUTPUT_FILENAME);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
            }

            Console.Write("Press any key to continue . . .");
            Console.ReadKey(true);
        }

        private static void ShiftOutput(List<List<string>> output, int periodInMonths)
        {
            if (periodInMonths > 0)
            {
                output[0].RemoveAt(output[0].Count() - 1);

                for (int actualPeriod = 1; actualPeriod <= periodInMonths; actualPeriod++)
                {
                    output[0].Add(string.Format(OUTPUT_HEADER_NAME, actualPeriod));

                    int lastColumnIndex = output[0].Count() - 1;

                    // 1 to jump the header
                    for (int i = 1; i < output.Count() - periodInMonths; i++)
                    {
                        int shiftRowIndex = i + 1;
                        output[i].RemoveAt(lastColumnIndex);
                        output[i].Add(output[shiftRowIndex].Last());
                    }

                    output.RemoveAt(output.Count() - 1);

                    //Copy last column
                    if(actualPeriod + 1 <= periodInMonths)
                    {
                        for(int i = 1; i < output.Count(); i++)
                        {
                            string last = output[i].Last();
                            output[i].Add(last);
                        }
                    }
                }
            }
        }

        private static void ExportCsv(List<List<string>> output, string filenameCsv)
        {
            Console.WriteLine(string.Format("Creating '{0}'...", filenameCsv));

            string outputFile = Path.Combine(Directory.GetCurrentDirectory(), OUTPUT_FILENAME);

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(outputFile))
            {
                string line = string.Empty;
                foreach (string headerValue in output.First())
                    line += string.Format("\"{0}\",", headerValue.Replace('.', '_').Replace(',', '_'));
                line = line.Remove(line.Count() - 1);
                file.WriteLine(line);

                foreach (List<string> row in output.Skip(1))
                {
                    line = string.Empty;
                    foreach (string value in row)
                        line += (value.Replace(',', '.') + ",");

                    line = line.Remove(line.Count() - 1);

                    file.WriteLine(line);
                }

                file.Close();
                file.Dispose();
            }

            Console.WriteLine(string.Format("Exported to '{0}'", outputFile));
        }
    }
}
